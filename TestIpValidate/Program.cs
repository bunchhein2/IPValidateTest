﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
namespace IPaddressDemo
{
    class ExampleValidateIP
    {
        public static void Main()
        {
            IPAddress IP;
            Console.WriteLine("Enter the IP Address: ");
            string ipAddress = Console.ReadLine();
            bool ipAdd = IPAddress.TryParse(ipAddress, out IP);
            if (ipAdd)
                Console.WriteLine("{0} is a valid IP address", ipAddress);
            else
                Console.WriteLine("{0} is a invalid IP address", ipAddress);
        }
    }
}